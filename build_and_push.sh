#!/bin/bash

set -euo pipefail

DOCKER_REPO=intangiblerealitieslab

with_push=false

for option in "$@"; do
    if [[ "$option" == "--push" ]]; then
        with_push=true
    else
        echo "Unknown option '$option'." >&2
        exit 1
    fi
done


image_list=list.tmp
rm -f ${image_list}
touch ${image_list}

find ./ -name Dockerfile | while read path; do
    name=$(echo $path | cut -f 2 -d /)
    full_name=${DOCKER_REPO}/$name
    docker build -t ${full_name} $(dirname $path)
    echo ${full_name} >> ${image_list}
done

if [[ ${with_push} == true ]]; then
    cat ${image_list} | while read full_name; do
        docker push ${full_name}
    done
fi

rm -f ${image_list}
