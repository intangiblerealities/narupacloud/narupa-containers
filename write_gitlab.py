#!/usr/bin/env python
# Copyright (C) 2021  University of Bristol
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Generate the .gitlab-ci.yml file.

The gitlab CI configuration needs to contain an entry per docker image. This
script lists all the docker images we want to build and generate the
configuration file for them.

Just run the script without arguments to write the file. If a configuration
file exists, it will be overwritten.
"""

from pathlib import Path
from jinja2 import Template

with open('templates/gitlab-ci.yml') as infile:
    template = Template(infile.read())

with open('ci-exclude.txt') as infile:
    exclusions = [name.strip() for name in infile.readlines()]

print(exclusions)

names = []
for dockerfile in Path('.').glob('./**/Dockerfile'):
    name = dockerfile.parent.name
    if name not in exclusions:
        names.append(name)

with open('.gitlab-ci.yml', 'w') as outfile:
    outfile.write(template.render(images=names))
