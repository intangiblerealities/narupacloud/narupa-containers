#!/usr/bin/python
# Copyright (C) 2021  University of Bristol
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Check that the all images in the repository are known by CI.

Check that all the images describes in the repository appear either in
.gitlab-ci.yml or ci-exclude.txt. Exit with a non-zero code if any image is
missing.
"""

from pathlib import Path
import sys

non_images = {'variables', 'check_images', 'stages'}

with open('.gitlab-ci.yml') as infile:
    keys = [
        line.strip()[:-1]
        for line in infile
        if not line.startswith(' ') and line.strip().endswith(':')
    ]

with open('ci-exclude.txt') as infile:
    keys.extend([
        line.strip()
        for line in infile
    ])

known_images = set(keys) - non_images

found_images = set(
    dockerfile.parent.name
    for dockerfile in Path('.').glob('./**/Dockerfile')
)

missing_images = found_images - known_images

if missing_images:
    print(
        f'The following images are not accounted for in CI: {missing_images}.',
        file=sys.stderr,
    )
    print(
        'Run "write_gitlab.py" or add the image name in "ci-exclude.txt".',
        file=sys.stderr,
    )
    sys.exit(1)
